
import os
import sys
import argparse
import logging
import pandas as pd
import sqlalchemy


def _get_config(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', dest='host', type=str)
    parser.add_argument('--port', dest='port', type=int)
    parser.add_argument('--username', dest='username', type=str)
    parser.add_argument('--password', dest='password', type=str)
    parser.add_argument('--database', dest='database', type=str)
    parser.add_argument('--schema', dest='schema', type=str)
    parser.add_argument('--table', dest='table', type=str)
    parser.add_argument('--source', dest='source', type=str)
    parser.add_argument('--delimiter', dest='delimiter', type=str, default=',')
    config, _ = parser.parse_known_args(argv)

    return config


def _connect_db(config):
    """Create database connection

    :param config Configuration parameters
    """

    url = sqlalchemy.engine.URL.create(
                drivername="postgresql",
                username=config.username,
                password=config.password,
                host=config.host,
                port=config.port,
                database=config.database,
            )
    
    eng = sqlalchemy.create_engine(url)

    return eng.connect()


def _get_source_path(source):
    """Build absolute source file path

    :param source Source path
    """
    current_path = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(current_path, source)


def main(argv):
    try:
        config = _get_config(argv)
        conn = _connect_db(config)
        source_path = _get_source_path(config.source)
        try:
            with open(source_path, 'r') as f:
                df = pd.read_csv(f)
                df.to_sql(config.table, conn, schema=config.schema, if_exists='replace')
        finally:
            conn.close()
    except Exception as e:
        logging.exception(e)


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    main(sys.argv)
