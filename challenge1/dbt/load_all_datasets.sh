PROJECT_DIR="$(cd "$(dirname "$0")"; pwd -P)"

DATASETS=(airlines airports flights planes weather)

for DATASET in ${DATASETS[*]}
  do
    SOURCE="${PROJECT_DIR}/nyxa/seeds/nyc_${DATASET}.csv"

    python ${PROJECT_DIR}/load_dw.py \
    --host localhost \
    --port 5432 \
    --username postgres \
    --password 12345678 \
    --database dw_flights \
    --schema staging \
    --table $DATASET \
    --source $SOURCE
done
